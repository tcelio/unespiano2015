$(document).ready(function(){
    
   			$(function() {
				
				$('#UploadImages').uberuploadcropper({
					//---------------------------------------------------
					// uploadify options..
					//---------------------------------------------------
					'debug'		: true,
					'action'	: '../uploadCropArte/upload.php',
					'params'	: {},
					'allowedExtensions': ['jpg','jpeg','png','gif'],
					//'sizeLimit'	: 0,
					//'multiple'	: true,
					//---------------------------------------------------
					//now the cropper options..
					//---------------------------------------------------
					//'aspectRatio': 1, 
					'allowSelect': false,			//can reselect
					'allowResize' : false,			//can resize selection
					'setSelect': [ 0, 0, 400, 320 ],	//these are the dimensions of the crop box x1,y1,x2,y2
					//'minSize': [ 300, 300 ],		//if you want to be able to resize, use these
					//'maxSize': [ 100, 100 ],
					//---------------------------------------------------
					//now the uber options..
					//---------------------------------------------------
					'folder': '../img/art/',			// only used in uber, not passed to server
                                        'cropAction': '../uploadCropArte/crop.php',		// server side request to crop image
					'onComplete': function(imgs,data){ 
						var $PhotoPrevs = $('#PhotoPrevs');

						for(var i=0,l=imgs.length; i<l; i++){
							$PhotoPrevs.append('<img src="../img/art/'+ imgs[i].filename +'" />');
						}
					}
				});
				
			}); 
    
    
    
    
    
    
    
});