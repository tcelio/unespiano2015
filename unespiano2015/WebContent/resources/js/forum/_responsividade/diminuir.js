$(document).ready(function(){
    
		var b = $('body').width();
	
		if(b >= 1136){
			mostraInformativoLateral();
		}
		if(b <= 1135 && b >= 865){
			retiraInformativoLateral();
		}
		if(b <= 864 && b > 420){
			retiraBoxInsercaoMSG();
		}
		if(b <= 420){//sobra apenas uma coluna
				transformaDuasEmUmaColuna();
		}

	$(window).on("resize",function(event)//evento de diminuir o tamanho da tela
	{
		var main = $('#main').width();
		var body = $('body').width();
		
		var mainInt = parseInt(main);
		var bodyInt = parseInt(body);
		
		if(bodyInt >= 1136){//mostra tudo, inclusive o item de atualidades lateral
				mostraInformativoLateral();
		}
		if(bodyInt <= 1135 && bodyInt >= 865){//retira o item de atualidades lateral
				retiraInformativoLateral();
		}
		if(bodyInt <= 864 && bodyInt > 420){//retira o box de inser��o de texto.Fica so as duas colunas
				retiraBoxInsercaoMSG();
		}
		if(bodyInt <= 420){//sobra apenas uma coluna
				transformaDuasEmUmaColuna();
		}
		
	});
 
 
 
});


function mostraInformativoLateral()
{
	
	$('.atualizacoes').css({display:"block"});//mostra as atualizacoes
	$('#main').css({width:"inherit"});
	$('#main').css({left:"inherit"});
	$('#main').css({marginLeft:"20%"});
	$('.logoUnespiano').css({display:"block"});
	$('.containerContainer .nav-conteudos').css({left:"inherit"});
	$('.containerContainer .nav-conteudos').css({marginLeft:"21%"});			
	
}

function retiraInformativoLateral()//retira o informativo lateral
{									//x <= 1135 && x >= 865	
	
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('#main').css({width:"900px"});
	$('#main').css({left:"50%"});
	$('#main').css({marginLeft:"-450px"});
	$('.logoUnespiano').css({display:"none"});
	$('.art-escondido').css({display:"none"});
	$('.containerMenu2').css({display:"none"});
	$('.containerMenu').css({display:"block"});
	$('.containerContainer .nav-conteudos').css({left:"50%"});
	$('.containerContainer .nav-conteudos').css({marginLeft:"-425px"});
	$('.nav-conteudos').css({display:"block"});
	$('.nav-conteudos2').css({display:"none"});
	$('.search-list').css({display:"none"});
	$('.menu-list').css({display:"none"});
	$('.sombra').css({width:"900px"});
	$('.containerPrincipal').css({width:"900px"});
}
function retiraBoxInsercaoMSG() //retira o box de insercao de msg
{								//x <= 864 && x > 420
	
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('#main').css({marginLeft:"1%"});//deixa o item principal da tela com 1% de margem lateral
	$('#main').css({marginRight:"1%"});//deixa o item principal da tela com 1% de margem lateral
	$('.logoUnespiano').css({display:"none"});
	$('#main').css({width:"100%"});
	$('#main').css({left:"inherit"});
	$('.containerMenu2').css({display:"block"});
	$('.containerMenu').css({display:"none"});
	$('.containerPrincipal').css({width:"96%"});
	$('.sombra').css({width:"100%"});
	$('.art-escondido').css({display:"block"});
	$('.nav-conteudos').css({display:"none"});
	$('.nav-conteudos2').css({display:"block"});
	$('.search-list').css({display:"block"});
	$('.menu-list').css({display:"block"});
	$('body').css({paddingRight:"0px"});
	$('body').css({paddingLeft:"0px"});
}

function transformaDuasEmUmaColuna()//x <= 420
{
	
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('#main').css({marginLeft:"1%"});//deixa o item principal da tela com 1% de margem lateral
	$('.logoUnespiano').css({display:"none"});
	$('#accountmenu').css({display:"none"});
	$('.nav-conteudos').css({display:"none"});
	$('.nav-conteudos2').css({display:"block"});
	$('.search-list').css({display:"block"});
	$('.menu-list').css({display:"block"});
}
