$(document).ready(function(){


   			$(function() {
				
				$('#UploadImages').uberuploadcropper({ 
					//---------------------------------------------------
					// uploadify options..
					//---------------------------------------------------
					'debug'		: true,
					'action'	: '../uploadCropMural/upload.php',
					'params'	: {},
					'allowedExtensions': ['jpg','jpeg','png','gif'],
					//'sizeLimit'	: 0,
					//'multiple'	: true,
					//---------------------------------------------------
					//now the cropper options..
					//---------------------------------------------------
					//'aspectRatio': 1, 
					'allowSelect': false,			//can reselect
					'allowResize' : false,			//can resize selection
					'setSelect': [ 0, 0, 300, 270 ],	//these are the dimensions of the crop box x1,y1,x2,y2
					//'minSize': [ 300, 300 ],		//if you want to be able to resize, use these
					//'maxSize': [ 100, 100 ],
					//---------------------------------------------------
					//now the uber options..
					//---------------------------------------------------
					'folder': '../img/mural/',			// only used in uber, not passed to server
                                        'cropAction': '../uploadCropMural/crop.php',		// server side request to crop image
					'onComplete': function(imgs,data){ 
                                                $('.containerAlert div').fadeOut(1000);
                                                $('#PhotoPrevs img').remove();
                                                $('#PhotoPrevs').css({border:"5px solid grey"});
                                                var parabens = '<div class="alert-success fade in alertF4"><strong>Parabéns</strong>  Foto inserida com sucesso!!!</div>';
                                                
                                                 $('.containerAlert').html(parabens).hide().fadeIn(1000).delay(1000).fadeOut(1000);
                                                
                                                
						var $PhotoPrevs = $('#PhotoPrevs');
                                                
                                                

						for(var i=0,l=imgs.length; i<l; i++){
							$PhotoPrevs.append('<img style="width:150px; height:130px;" src="../img/mural/'+ imgs[i].filename +'"/>');
						}
					}
				});
				
			}); 
    
    
    
    
    
    
    
});