$(document).ready(function(){
    
		var b = $('body').width();
	
		if(b >= 1136){
			mostraInformativoLateral();
		}
		if(b <= 1135 && b >= 865){
			retiraInformativoLateral();
		}
		if(b <= 864 && b > 420){
			retiraBoxInsercaoMSG();
		}
		if(b <= 420){//sobra apenas uma coluna
				transformaDuasEmUmaColuna();
		}

	$(window).on("resize",function(event)//evento de diminuir o tamanho da tela
	{
		var main = $('#main').width();
		var body = $('body').width();
		
		var mainInt = parseInt(main);
		var bodyInt = parseInt(body);
		
		if(bodyInt >= 1136){//mostra tudo, inclusive o item de atualidades lateral
				mostraInformativoLateral();
		}
		if(bodyInt <= 1135 && bodyInt >= 865){//retira o item de atualidades lateral
				retiraInformativoLateral();
		}
		if(bodyInt <= 864 && bodyInt > 420){//retira o box de inser��o de texto.Fica so as duas colunas
				retiraBoxInsercaoMSG();
		}
		if(bodyInt <= 420){//sobra apenas uma coluna
				transformaDuasEmUmaColuna();
		}
		
	});
 
 
 
});


function mostraInformativoLateral()
{
	$('.containerPersonalLateral').css({display:"block"});//mostra o box de insercao de msg
	$('.atualizacoes').css({display:"block"});//mostra as atualizacoes
	$('#main').css({width:"inherit"});
	$('#main').css({left:"inherit"});
	$('#main').css({marginLeft:"20%"});
	$('.logoUnespiano').css({display:"block"});
	$('.containerContainer .nav-conteudos').css({left:"inherit"});
	$('.containerContainer .nav-conteudos').css({marginLeft:"21%"});			
	$('.containerCampus').css({width:"305px"});
	$('.containerUnesp').css({width:"305px"});
}

function retiraInformativoLateral()//retira o informativo lateral, porem mostra o box de insercao de msg
{
	$('.containerPersonalLateral').css({display:"block"});//mostra o box de insercao de msg
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('#main').css({width:"900px"});
	$('#main').css({left:"50%"});
	$('#main').css({marginLeft:"-450px"});
	$('.logoUnespiano').css({display:"none"});
	$('.containerContainer .nav-conteudos').css({left:"50%"});
	$('.containerContainer .nav-conteudos').css({marginLeft:"-425px"});
	$('.containerBarraTop').css({width:"610px"});
	$('.containerBarraTop').css({borderTopLeftRadius:"0px"});
	$('.top1').css({width:"305px"});
	$('.top2').css({width:"305px"});
	$('.containerCampus').css({width:"305px"});
	$('.containerUnesp').css({width:"305px"});
	$('.nav-conteudos').css({display:"block"});
	$('.nav-conteudos2').css({display:"none"});
	$('.search-list').css({display:"none"});
	$('.menu-list').css({display:"none"});
}
function retiraBoxInsercaoMSG() //retira o box de insercao de msg
{
	$('.containerPersonalLateral').css({display:"none"});//retira o box de insercao de msg
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('#main').css({marginLeft:"1%"});//deixa o item principal da tela com 1% de margem lateral
	$('#main').css({marginRight:"1%"});//deixa o item principal da tela com 1% de margem lateral
	$('.logoUnespiano').css({display:"none"});
	$('#main').css({width:"100%"});
	$('#main').css({left:"inherit"});
	$('.containerCampus').css({width:"49%"});
	$('.containerUnesp').css({width:"49%"});
	$('.containerUnesp').css({display:"block"});
	$('.containerBarraTop').css({width:"98%"});
	$('.containerBarraTop').css({borderTopLeftRadius:"5px"});
	$('.top1').css({width:"49%"});
	$('.top2').css({width:"49%"});
	$('.nav-conteudos').css({display:"none"});
	$('.nav-conteudos2').css({display:"block"});
	$('.search-list').css({display:"block"});
	$('.menu-list').css({display:"block"});
}

function transformaDuasEmUmaColuna()
{
	$('.containerPersonalLateral').css({display:"none"});//retira o box de insercao de msg
	$('.atualizacoes').css({display:"none"});//retira as atualizacoes
	$('.containerUnesp').css({display:"none"});//retira a coluna de msg da unesp toda
	$('.containerCampus').css({width:"98%"});//deixa apenas a coluna de msg do seu campus predileto
	$('.containerBarraTop').css({width:"98%"});
	$('.containerBarraTop').css({borderTopLeftRadius:"5px"});
	$('#main').css({marginLeft:"1%"});//deixa o item principal da tela com 1% de margem lateral
	
	$('.logoUnespiano').css({display:"none"});
	$('.top2').css({display:"none"});
	$('.top1').css({width:"98%"});
	$('#accountmenu').css({display:"none"});
	$('.nav-conteudos').css({display:"none"});
	$('.nav-conteudos2').css({display:"block"});
	$('.search-list').css({display:"block"});
	$('.menu-list').css({display:"block"});
}
