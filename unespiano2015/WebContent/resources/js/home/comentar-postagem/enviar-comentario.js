$(document).ready(function(){

			//keyup chamado apos o texto ser inserido
			//keydown chamado antes do evento em si ser inserido ou ocorrido
			
		$('.containerCampus').on('keydown', '.campo-comentario-h', function(e){
			
			var keycode = (e.keyCode ? e.keyCode : e.which);
			if((e.which === 13) && !e.shiftKey){

				var comentario = $(this).html();				
				var m_ = comentario.length;
				var id_postagem = $(this).parent('form').parent('.comentarioUser').attr('id');
				var id_usuario  = $('.containerPersonal').find('.containerIdentificacoes').find('#usuarioId').html();
				var num_comentarios = $(this).parent('form').parent('.comentarioUser').parent('.item-original').attr('numero_comentarios');
				
				if(m_ == 0){
					e.preventDefault();
				}  
				if(m_ > 0){
					e.preventDefault();

					enviar_comentario(comentario, id_postagem, id_usuario, num_comentarios);
				}

			}
		});
		

			
			
				function enviar_comentario(comentario, id_postagem, id_usuario, num_comentarios){
		
		$.ajax({
                     type:"POST",
                     dataType:'json',
                     url:'../_CONTROL/home/gravar-comentario-banco-dados.php',
                     data:{ 
		                           id_usuario:id_usuario,
		                           comentario:comentario,
		                           id_postagem:id_postagem,
		                           num_comentarios:num_comentarios
                         },
                     beforeSend: function(){
                     			$('.containerCampus').find('#'+id_postagem).find('.comentarioUser').find('.campo-comentario-h').css({display:"none"});
								$('.containerCampus').find('#'+id_postagem).find('.comentarioUser').find('.avatar-msg-k').css({display:"none"});
								$('.containerCampus').find('#'+id_postagem).find('.loading-gif-comentario').css({display:"block"});
                     },
                     success: function(data){
                     	$('.containerCampus').find('#'+id_postagem).find('.comentarioUser').find('.campo-comentario-h').empty();
                     	$('.containerCampus').find('#'+id_postagem).find('.comentarioUser').find('.campo-comentario-h').css({display:"block"});
						$('.containerCampus').find('#'+id_postagem).find('.comentarioUser').find('.avatar-msg-k').css({display:"block"});
						$('.containerCampus').find('#'+id_postagem).find('.loading-gif-comentario').css({display:"none"});
						
						var nome 		= data.nome;
						var comentario  = data.comentario;
						var foto_avatar  = data.foto_avatar;
						var num_comentarios = data.num_comentarios;
						
							$('.containerCampus').find('#'+id_postagem).attr('numero_comentarios', num_comentarios);
							$('.containerCampus').find('#'+id_postagem).find('.comentariosOutros').find('.comentar').find('.comentariosValue').html('('+num_comentarios+')');
												
						inserir_comentario_pos(nome, comentario, foto_avatar, id_postagem);

                                    },
                     complete:function(retorno){
							
						
        	                            },
                     error: function(erro){
                                        
                    }         
              });  
		                            return false;
	
	
	};
			
			
			
			
		
});

inserir_comentario_pos = function(nome, comentario, foto_avatar, id_postagem){
		 						
                                      var html = "";
                					html += "<div class='container-comentario-h'><div class='container-foto-pp'><img src='"+foto_avatar+"'/></div>";
                					html += "<div class='container-comentario-pp'><span class='nome-comentario-pp'>"+nome+"</span>";
                					html += comentario+"</div></div>";
                					
            
              $('.containerCampus').find('#'+id_postagem).find('.comentarioUser2').prepend(html);
              $('.containerCampus').find('#'+id_postagem).find('.comentarioUser2').find('container-comentario-h:first-child').hide().fadeIn(1000);
            
		
	};