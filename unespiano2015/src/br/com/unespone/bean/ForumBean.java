package br.com.unespone.bean;

public class ForumBean {

	private String titulo;
	private String id;//id da publicacao
	private String texto;
	private String usuarioId;
	private String fotoAvatar;
	private String fotoGrande;
	private String tipo; //campus, curso, universidade, campus-curso
	private int numComentarios;//numero de comentarios	
	private String dataPublicacao; //nao sei se eh melhor usar String ou Date
	private int numNovasPostagens;//numero de novas postagens desde um periodo, como "total do dia"..
	
	//Abaixo eh necessario colocar afim de otimizacao:
	private String campus;
	private String curso;
	private String universidade;
	private String campusId;
	private String cursoId;
	private String universidadeId;
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getFotoAvatar() {
		return fotoAvatar;
	}
	public void setFotoAvatar(String fotoAvatar) {
		this.fotoAvatar = fotoAvatar;
	}
	public String getFotoGrande() {
		return fotoGrande;
	}
	public void setFotoGrande(String fotoGrande) {
		this.fotoGrande = fotoGrande;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getNumComentarios() {
		return numComentarios;
	}
	public void setNumComentarios(int numComentarios) {
		this.numComentarios = numComentarios;
	}
	public String getDataPublicacao() {
		return dataPublicacao;
	}
	public void setDataPublicacao(String dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	public int getNumNovasPostagens() {
		return numNovasPostagens;
	}
	public void setNumNovasPostagens(int numNovasPostagens) {
		this.numNovasPostagens = numNovasPostagens;
	}
	public String getCampus() {
		return campus;
	}
	public void setCampus(String campus) {
		this.campus = campus;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getUniversidade() {
		return universidade;
	}
	public void setUniversidade(String universidade) {
		this.universidade = universidade;
	}
	public String getCampusId() {
		return campusId;
	}
	public void setCampusId(String campusId) {
		this.campusId = campusId;
	}
	public String getCursoId() {
		return cursoId;
	}
	public void setCursoId(String cursoId) {
		this.cursoId = cursoId;
	}
	public String getUniversidadeId() {
		return universidadeId;
	}
	public void setUniversidadeId(String universidadeId) {
		this.universidadeId = universidadeId;
	}
	
	
}
