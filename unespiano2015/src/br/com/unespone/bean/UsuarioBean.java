package br.com.unespone.bean;

public class UsuarioBean {

	private String usuarioId;
	private String nome;
	private String sobreNome;
	
	private String campusNome;
	private String campusId;
	
	private String cursoNome;
	private String cursoId;
	
	private String fotoAvatar;
	private String republica;
	private String turmaNome;//frase que lembra a turma...tipo turma do barulho ou sei la...
	private String turmaId;
	private String universidade;
	private String universidadeId;
	
	private String cidadeId;
	private String cidadeNome;
	
	private String descricao;
	private boolean ativo;
	private String sexo;
	private String idade;
	private String estadoCivil;
	public String getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobreNome() {
		return sobreNome;
	}
	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}
	public String getCampusNome() {
		return campusNome;
	}
	public void setCampusNome(String campusNome) {
		this.campusNome = campusNome;
	}
	public String getCampusId() {
		return campusId;
	}
	public void setCampusId(String campusId) {
		this.campusId = campusId;
	}
	public String getCursoNome() {
		return cursoNome;
	}
	public void setCursoNome(String cursoNome) {
		this.cursoNome = cursoNome;
	}
	public String getCursoId() {
		return cursoId;
	}
	public void setCursoId(String cursoId) {
		this.cursoId = cursoId;
	}
	public String getFotoAvatar() {
		return fotoAvatar;
	}
	public void setFotoAvatar(String fotoAvatar) {
		this.fotoAvatar = fotoAvatar;
	}
	public String getRepublica() {
		return republica;
	}
	public void setRepublica(String republica) {
		this.republica = republica;
	}
	public String getTurmaNome() {
		return turmaNome;
	}
	public void setTurmaNome(String turmaNome) {
		this.turmaNome = turmaNome;
	}
	public String getTurmaId() {
		return turmaId;
	}
	public void setTurmaId(String turmaId) {
		this.turmaId = turmaId;
	}
	public String getUniversidade() {
		return universidade;
	}
	public void setUniversidade(String universidade) {
		this.universidade = universidade;
	}
	public String getUniversidadeId() {
		return universidadeId;
	}
	public void setUniversidadeId(String universidadeId) {
		this.universidadeId = universidadeId;
	}
	public String getCidadeId() {
		return cidadeId;
	}
	public void setCidadeId(String cidadeId) {
		this.cidadeId = cidadeId;
	}
	public String getCidadeNome() {
		return cidadeNome;
	}
	public void setCidadeNome(String cidadeNome) {
		this.cidadeNome = cidadeNome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	 
	
	
	
	
}
