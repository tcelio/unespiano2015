package br.com.unespone.bean;

public class EsporteBean {

	private String titulo;
	private String id;//id da publicacao
	private String texto;//explicando o campeonato
	private String usuarioId;//criador do campeonato
	private String fotoAvatar;// usado
	private String fotoGrande;
	private String tipo; //musica, literatura, escultura, pintura, arquitetura,danca, cinema, fotografia, quadrinhos.
	private int numComentarios;//numero de comentarios	
	private String preco;// so se estiver a venda
	private String dataPublicacao; //nao sei se eh melhor usar String ou Date
	private int numNovasPostagens;//numero de novas postagens desde um periodo, como "total do dia"..
	
	//Abaixo eh necessario colocar afim de otimizacao:
	private String campus;
	private String curso;
	private String universidade;
	private String campusId;
	private String cursoId;
	private String universidadeId;
}
