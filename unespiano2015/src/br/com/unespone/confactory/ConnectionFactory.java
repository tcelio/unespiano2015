package br.com.unespone.confactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionFactory {

	private final static String Driver = "org.postgresql.Driver";
	private final static String URL = "jdbc:postgresql://localhost:5432/unespiano";
	private final static String USER = "postgres";
	private final static String PASSWORD = "1234";
	//private static Connection conn;
	
	public ConnectionFactory(){
		
		try{
			Class.forName(Driver);
			System.out.println("Aqui conectou legal!!");
		}catch(java.lang.ClassNotFoundException e){
			System.out.println("Driver nao selecionado: "+e.getMessage());
		}
	}
	
	
	public static Connection getConection() throws SQLException{
		
		Connection con = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			System.out.println("Aqui conectou legal!!"+con);
			return con;
		} catch (Exception e) {
			System.out.println("Falha ao conectar no banco de dados: "+e.getMessage());
		}
		return con;		
	}
	
	  // FECHA AS CONEXOES JAVA
	public static void closeConnection(Connection conn, PreparedStatement pstm, ResultSet rs) throws Exception {
	        close(conn, pstm, rs);
	    }
	 
	    public static void closeConnection(Connection conn, PreparedStatement pstm) throws Exception {
	        close(conn, pstm, null);
	    }
	 
	    public static void closeConnection(Connection conn) throws Exception {
	        close(conn, null, null);
	    }
	 
	    private static void close(Connection conn, PreparedStatement pstm, ResultSet rs) throws Exception {
	        try {
	            if (rs != null) rs.close( );
	            if (pstm != null)pstm.close( );
	            if (conn != null)conn.close( );
	        } catch (Exception e) {
	            throw new Exception(e.getMessage( ));
	        }
	    }    
	
	
}
