package br.com.unespone.dao;

import java.sql.Connection;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.unespone.bean.ArteBean;
import br.com.unespone.confactory.ConnectionFactory;
import br.com.unespone.interfaces.PostagemComentario;

public class ArteDAO extends ConnectionFactory implements PostagemComentario {

	//private int paramArte;
	private Connection connection;
	
	public final static String SELECT_POSTAGENS_DEFAULT = "SELECT * FROM Arte LIMIT 1";//exibe as postagens ao carregar a pagina (20 postagens)
	public final static String SELECT_POSTAGENS = "SELECT * FROM Arte"; 
	public final static String SELECT_UMA_POSTAGEM = "SELECT * FROM Arte"; 
	public final static String salvarPostagemArte = "SELECT * FROM Arte";
	public final static String deletarPostagemArte = "SELECT * FROM Arte";
	public final static String atualizarPostagemArte = "SELECT * FROM Arte";
	public final static String salvarComentarioArte = "SELECT * FROM Arte";
	public final static String deletarComentarioArte = "SELECT * FROM Arte";
	public final static String atualizarComentarioArte = "SELECT * FROM Arte";
	public final static String insertGosteiOuNaoGostei = "SELECT * FROM Arte";
	
	public ArteDAO() throws SQLException{
		this.connection = ConnectionFactory.getConection();
	}
	
	
	public List<ArteBean> selecionarPostagens() {
			
		List<ArteBean> arte = new ArrayList<ArteBean>();
		try {
			
			PreparedStatement stmt = connection.prepareStatement(SELECT_POSTAGENS_DEFAULT);
			ResultSet rs = stmt.executeQuery();
			stmt.execute();
			
			while(rs.next()){
				ArteBean arteBean = new ArteBean();
				
				arteBean.setTitulo(rs.getString("titulo"));
				arteBean.setId(rs.getString("id"));
				arteBean.setUsuarioId(rs.getString("usuarioId"));
				arteBean.setNome(rs.getString("nome"));
				arteBean.setSobreNome(rs.getString("sobrenome"));
				arteBean.setUniversidade(rs.getString("universidade"));
				arteBean.setCampus(rs.getString("campus"));
				arteBean.setTipo(rs.getString("tipo"));
				arteBean.setFotoAvatar(rs.getString("fotoAvatar"));
				arteBean.setFotoGrande(rs.getString("fotoGrande"));
											
				arte.add(arteBean);
			}
			stmt.close();
		
		} catch (SQLException e) {
			 System.out.println("Erro: "+e.getMessage());
		}
		return arte;
	}


	public String selecionarUmaPostagem() {
		
		String arte = null;
		
		//ArteBean arteBean = new ArteBean();
		System.out.println("chegpou aqui!!");
		try{
				PreparedStatement stmt = connection.prepareStatement(SELECT_UMA_POSTAGEM);
				ResultSet rs = stmt.executeQuery();
				stmt.execute();
				while(rs.next()){
					arte = rs.getString("campus_nome");
				}
				/*arteBean.setCurso(rs.getString("curso_nome"));
				arteBean.setDataPublicacao(rs.getString("data_post"));
				//arteBean.setFotoAvatar(rs.getString(""));
				arteBean.setFotoGrande(rs.getString("foto"));
				arteBean.setNome(rs.getString("usuario_nome"));
				//arteBean.setNumComentarios(rs.getInt());
				//arteBean.setSobreNome(rs.getString(""));
				arteBean.setTexto(rs.getString("descricao"));
				arteBean.setTipo(rs.getString("id_arte_tipo"));
				arteBean.setTitulo(rs.getString("titulo"));
				arteBean.setUniversidade(rs.getString("id_universidade"));
				arteBean.setUsuarioId(rs.getString("id_user"));*/
				
		}catch(Exception e){
			System.out.println("Erro: "+e.getMessage());
		}finally{
			
		}
		return arte;
	}


	public List<ArteBean> selecionarPostagensDefault() {
		
		List<ArteBean> arte = new ArrayList<ArteBean>();
		
		try{
		
			PreparedStatement stmt = connection.prepareStatement(SELECT_POSTAGENS_DEFAULT);
			ResultSet rs =stmt.executeQuery();
			stmt.execute();
			
			while(rs.next()){
				ArteBean arteBean = new ArteBean();
				
				arteBean.setCampus(rs.getString("campus_nome"));
				arteBean.setCurso(rs.getString("curso_nome"));
				arteBean.setDataPublicacao(rs.getString("data_post"));
				//arteBean.setFotoAvatar(rs.getString(""));
				arteBean.setFotoGrande(rs.getString("foto"));
				arteBean.setNome(rs.getString("usuario_nome"));
				//arteBean.setNumComentarios(rs.getInt());
				//arteBean.setSobreNome(rs.getString(""));
				arteBean.setTexto(rs.getString("descricao"));
				arteBean.setTipo(rs.getString("id_arte_tipo"));
				arteBean.setTitulo(rs.getString("titulo"));
				arteBean.setUniversidade(rs.getString("id_universidade"));
				arteBean.setUsuarioId(rs.getString("id_user"));
											
				arte.add(arteBean);
			}
			stmt.close();
			
		}catch(Exception e){
			System.out.println("Erro: "+e.getMessage());
		}finally{
			
		}
		
		return arte;
	}
	
	@Override
	public boolean salvarPostagem() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deletarPostagem() {
		return false;
	}

	@Override
	public boolean atualizarPostagem() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean salvarComentario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deletarComentario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean atualizarComentario() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String gostei() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
