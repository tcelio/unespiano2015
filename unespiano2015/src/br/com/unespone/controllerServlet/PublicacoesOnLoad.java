package br.com.unespone.controllerServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PublicacoesOnLoad
 */
@WebServlet("/PublicacoesOnLoad")
public class PublicacoesOnLoad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublicacoesOnLoad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//out.println("Bem Vindo<h3>"+value+"</h3>");
		//out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String value = request.getParameter("campus_id");
		response.setContentType("text");
        response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = response.getWriter();
		out.println("Bem Vindo<h3>"+value+"</h3>");
		out.close();
		
		
	}

}
