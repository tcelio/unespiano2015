package br.com.unespone.controllerServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.unespone.dao.ArteDAO;

/**
 * Servlet implementation class Arte
 */
@WebServlet(name = "arte", urlPatterns = { "/arte" })
public class Arte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Arte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			String arte = new ArteDAO().selecionarUmaPostagem();
			request.setAttribute("arte", arte);
			request.getRequestDispatcher("/arte/final.jsp").forward(request, response);
			
			
		}catch(Exception e){
			System.out.println("Erro: "+e.getMessage());
		}
		
	}

}
