package br.com.unespone.interfaces;

import java.util.List;

public interface PostagemComentario {
	
	//public List<String> selecionarPostagens();
	//public String selecionarUmaPostagem();
	//public String selecionarPostagensDefault();
	public boolean salvarPostagem();
	public boolean deletarPostagem();
	public boolean atualizarPostagem();
	
	//public List<String> selecionarMensagens();
	public boolean salvarComentario();
	public boolean deletarComentario();
	public boolean atualizarComentario();
	
	public String gostei();//sim ou nao
	//public boolean 
	
}
